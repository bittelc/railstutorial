=begin

Link: https://www.railstutorial.org/book/beginning#sec-up_and_running

IDE and local development vs cloud
For installing rails and completing project locally, use http://installrails.com/ for help
For using an IDE, Cloud9 is one through which tutorial is reliant
Three essential components to develop web apps:
  Text Editor
  Filesystem Navigator
  Command Line

I will be using the IDE so I can move quicker
It's standard Ruby practice to have 2 space indentation instead of 4
