=begin

Link: https://www.railstutorial.org/book/beginning

Rails created by David Heinemeier Hansson

For projects, will be using standard development environment in the cloud
Will be using built-in MiniTest testing framework
Will eliminate many external dependencies (Rspec, Cucumber, Capybara, Factory Girl)

Will put our first app on the web by deploying it to production
Will get help from "scaffolding" to generate code for second app, "toy_app"
Third app will be like twitter. Will write all the code from scratch. Will use mockups, test-driven development, and integration tests.
	Static pages -> dynamic content
	Site layout -> user data model -> full registration and authentication system -> microblogging and social features

# =======
Scaffolding:
Scaffolding is Rails generated code created by Rails.
$ generate scaffold
Complexity and sheer amount of code in scaffold can be overwhelming. 
Will thus (nearly) exempt ourselves from the scaffolding technique so that we can learn the core of Rails language.

=end
