=begin

Link: https://www.railstutorial.org/book/beginning#sec-introduction

Rails framework created in 2004
Rails is 100% open-source
Exploits malleability of Ruby language, by effectively creating a domain specific language (DSL on wikipedia)
	Thus, web programming tasks like generating HTML, making data models, and routing URL's is easy

Rails core programming team adopts best practice very quickly, and merged with Merb (a rival Ruby framework) for a modular design, stable API and improved performance

#---------
Convention
	Code that results in a failing test is written in red, whereas code written in a passing test is written in green
	Dots on textEditor lines indicate omitted code
		Public class
			.
			.
			.
			puts "code"
		end

=end
